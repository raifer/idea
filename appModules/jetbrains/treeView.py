# coding: utf-8

import controlTypes
import ui
from NVDAObjects.JAB import JAB


class TreeView(JAB):
	def event_gainFocus(self):
		msg = self.name
		if (controlTypes.STATE_COLLAPSED in self.states):
			msg += u" réduit"
		elif (controlTypes.STATE_EXPANDED in self.states):
			msg += u" développé"

		info = self._get_positionInfo()
		msg += " niveau %d, %d sur %d" % (info["level"], info["indexInGroup"], info["similarItemsInGroup"])
		ui.message(msg)

	def event_stateChange(self):
		if (controlTypes.STATE_SELECTED not in self.states):
			return

		if (controlTypes.STATE_COLLAPSED in self.states):
			ui.message(u"réduit")
		elif (controlTypes.STATE_EXPANDED in self.states):
			ui.message(u"développé, %d éléments" % self.childCount)
		else:
			super(JAB, self).event_stateChange()
