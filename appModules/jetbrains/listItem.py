# coding: utf-8

import controlTypes
from NVDAObjects.JAB import JAB


class ListItem(JAB):
	def _get_name(self):
		return ListItem.find_obj_to_build_name(self)

	def build_name(obj):
		name = obj.description
		if obj.childCount >= 1 and obj.children[0].name:
			if name:
				if name[-1] == '.':
					name += " "
				else:
					name += ". "
			name += obj.children[0].name
		if obj.childCount >= 2 and obj.children[1].name:
			if name:
				name += ". "
			name += obj.children[1].name
		if obj.childCount >= 3 and obj.children[2].name:
			if name:
				name += ". "
			name += obj.children[2].name
		return name

	def find_obj_to_build_name(obj):
		if (obj.description
				and obj.childCount
				and obj.children[0].role == controlTypes.ROLE_STATICTEXT):
			return ListItem.build_name(obj)
		if (obj.role != controlTypes.ROLE_LISTITEM
				and obj.name):
			return obj.name
		if obj.childCount:
			for child in obj.children:
				name = ListItem.find_obj_to_build_name(child)
				if name:
					return name
		return ""

	find_obj_to_build_name = staticmethod(find_obj_to_build_name)
	build_name = staticmethod(build_name)
