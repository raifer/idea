# coding: utf-8

import api
from NVDAObjects.JAB import JAB


class EditableList(JAB):
	def event_gainFocus(self):
		import tones
		tones.beep(440, 100)

	# if (controlTypes.STATE_OFFSCREEN in self.states):
	# api.setFocusObject(self._get_parent()._get_parent())
	# else:
	# ui.message(self.name)

	def script_recupEditorFocus(self, gesture):
		api.setFocusObject(self._get_parent()._get_parent())
		gesture.send()

	__gestures = {
		"kb:Escape": "recupEditorFocus",
	}
