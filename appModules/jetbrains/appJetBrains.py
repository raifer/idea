# coding: utf-8

import appModuleHandler
import controlTypes
import api
import ui
from scriptHandler import script

from autocompletionList import AutocompletionList
from listItem import ListItem
from textArea import TextArea
from treeView import TreeView

class AppJetBrains(appModuleHandler.AppModule):
	lastTextArea = ""

	@script(
		gesture="kb:NVDA+t"
	)
	def script_announceTitle(self, gesture):
		obj = api.getForegroundObject()
		if self.lastTextArea:
			ui.message(self.lastTextArea.name)
		ui.message(obj.name)

	def chooseNVDAObjectOverlayClasses(self, obj, clsList):
		# Overlay autocompletion list
		if (obj.role == controlTypes.ROLE_LISTITEM
				and obj.childCount == 3
				and obj.name
				# and obj.parent.parent.role == controlTypes.ROLE_EDITABLETEXT
		):
			clsList.insert(0, AutocompletionList)

		# Overlay text area
		if (obj.role == controlTypes.ROLE_EDITABLETEXT
				and not obj.childCount
				and obj.isFocusable):
			clsList.insert(0, TextArea)
			self.lastTextArea = obj

		# Overlay tree view
		if obj.role == controlTypes.ROLE_TREEVIEWITEM:
			clsList.insert(0, TreeView)

		# Overlay editable list without name like find action
		if (obj.role == controlTypes.ROLE_LISTITEM
				and not obj.name
				and obj.childCount
		):
			clsList.insert(0, ListItem)
