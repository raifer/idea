# coding: utf-8

import controlTypes
import speech
import textInfos
import ui
from NVDAObjects.behaviors import EditableTextWithoutAutoSelectDetection
from scriptHandler import script


class TextArea(EditableTextWithoutAutoSelectDetection):
	def _get_name(self):
		# Remove "Editable for" text at the beginning of name.
		name = super(EditableTextWithoutAutoSelectDetection, self)._get_name()
		if name[:6] == "Editor":
			name = name[10:]
		return name

	@script(
		gestures=["kb:alt+shift+upArrow", "kb:control+shift+upArrow"]
	)
	def script_sendGestureAndAnnouncePreviousLine(self, gesture):
		gesture.send()
		info = self.makeTextInfo(textInfos.POSITION_CARET)
		caretOffset = info._getCaretOffset()
		lineOffset, _ = info._getLineOffsets(caretOffset)
		info.move(textInfos.UNIT_CHARACTER, lineOffset - caretOffset - 1)
		info.expand(textInfos.UNIT_LINE)
		speech.speakTextInfo(info, unit=textInfos.UNIT_LINE, reason=controlTypes.REASON_CARET)

	@script(
		gestures=["kb:control+shift+downArrow", "kb:alt+shift+downArrow"]
	)
	def script_sendGestureAndAnnounceNextLine(self, gesture):
		gesture.send()
		info = self.makeTextInfo(textInfos.POSITION_CARET)
		info.move(textInfos.UNIT_LINE, 1)
		info.expand(textInfos.UNIT_LINE)
		speech.speakTextInfo(info, unit=textInfos.UNIT_LINE, reason=controlTypes.REASON_CARET)

	__gestures = {
		"kb:control+x": "caret_moveByLine",
		"kb:control+y": "caret_moveByLine",
		"kb:control+b": "caret_moveByLine",
		"kb:alt+upArrow": "caret_moveByLine",
		"kb:alt+downArrow": "caret_moveByLine",
		"kb:control+d": "caret_moveByLine",
		"kb:control+:": "caret_moveByLine",
		"kb:control+w": "caret_changeSelection",
	}
