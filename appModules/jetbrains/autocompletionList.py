# coding: utf-8

import api
import controlTypes
import ui
from NVDAObjects.JAB import JAB


class AutocompletionList(JAB):
	def event_gainFocus(self):
		if (controlTypes.STATE_OFFSCREEN in self.states):
			api.setFocusObject(self._get_parent()._get_parent())
		else:
			ui.message(self.name)

	def script_recupEditorFocus(self, gesture):
		api.setFocusObject(self._get_parent()._get_parent())
		gesture.send()

	__gestures = {
		"kb:Escape": "recupEditorFocus",
	}
