# Module complémentaire de la suite JetBrains - manuel utilisateur #

*	Auteur : Mathieu Barbe <mathieu@kolabnow.com>
*	URL : 
* Compatibilité : testé avec CLion 2018.3


## Fonctionnalités ##

### Zone d'édition

* Récupération du focus dans la zone d'édition après avoir validé ou quité la liste d'autocomplétion,
* `NVDA + t` annonce le nom du fichier en cours d'édition,
* Lecture de la ligne courante après les actions suivantes :
    - supression de la ligne sous le curseur, `control + y`,
    - couper la ligne sous le curseur, `control + x`,
    - se déplacer jusqu'à la fonction suivante, `alt + bas`,
    - se déplacer jusqu'à la fonction précédente, `alt + haut`.

### Parcours d'arborescence

* Réduction de la verbositée lors du parcours de l'arbre;
* Anonce le nombre d'éléments lors de l'ouverture d'un nœud.

## Historique ##
### v1.0 (4 mars 2019)

## A venir

* Titre de la fenêtre en global avec : nom du fichier, nom du projet et nom de l'application